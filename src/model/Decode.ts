import { DeviceName } from './Device'

export enum DecodingAction {
    GET_INFO = 'getInfo',
    UNREGISTER = 'unregister',
    VIRGIN = 'virgin',
    CHANGE_VIN = 'changeVin',
    RESET_COUNTER = 'reset-counter',
}

export interface DecodingConfig {
    name: DeviceName
    actions: DecodingAction[]
}
