export enum CarManufacturer {
    OPEL_VAUXHALL = 'opel',
    PEUGEOT_CITROEN = 'psa',
}

export interface Manufacturer {
    imageUrl: string
    brand: string
    description: string
}

export interface ManufacturerMenuItem extends Manufacturer {
    url: string
}
