import { OpelVauxhallDisplayName, OpelVauxhallRadioName } from '../db/devices/opelVauxhall'
import { PeugeotCitroenRadioName } from '../db/devices/peugeotCitroen'

export enum InvalidRadioName {
    INVALID = 'invalid',
}

export type DeviceName = OpelVauxhallRadioName | OpelVauxhallDisplayName | PeugeotCitroenRadioName | InvalidRadioName

export enum DeviceType {
    RADIO = 'radio',
    DISPLAY = 'display',
}

export interface Device {
    model: DeviceName
    type: DeviceType
    description: string
    eepromType: string
    image: string
    procedureDescription: string
}

export interface DeviceParams {
    deviceType: DeviceType
    deviceName: DeviceName
}
