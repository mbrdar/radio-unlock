export interface DumpInfo {
    code: string
    counter?: number
    serialNumber?: string
}
