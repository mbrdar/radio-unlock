import axios from 'axios'
import { apiUrl } from './environment'

export const herokuWakeUp = (): void => {
    const url = apiUrl().replace('/api/v1', '/actuator/health')

    axios.get(url)
}
