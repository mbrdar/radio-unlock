import { AxiosError } from 'axios'

const getErrorMessage = (error: AxiosError, alternativeMessage: string): string => {
    return (error.response && error.response.data && error.response.data.message) || alternativeMessage
}

export default getErrorMessage
