export function apiUrl(): string {
    let url = 'http://localhost:8080/api/v1'
    if (process.env.NODE_ENV === 'production') {
        if (process.env.REACT_APP_API_URL) {
            url = process.env.REACT_APP_API_URL
        } else {
            throw new Error('Api url is not configured')
        }
    }
    return url
}
