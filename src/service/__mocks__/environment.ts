export const TEST_API_URL = 'http://localhost:8080/api/v1'

export function apiUrl(): string {
  return TEST_API_URL
}
