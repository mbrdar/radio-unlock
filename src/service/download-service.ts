export const downloadFile = (blobParts: BlobPart[], fileName: string): void => {
    const url = window.URL.createObjectURL(new Blob(blobParts))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download', fileName)
    document.body.appendChild(link)
    link.click()
}

export const getFileNameFromContentDispositionHeader = (contentDisposition: string): string => {
    if (contentDisposition) {
        const startFileNameIndex = contentDisposition.indexOf('"') + 1
        const endFileNameIndex = contentDisposition.lastIndexOf('"')
        return contentDisposition.substring(startFileNameIndex, endFileNameIndex)
    }
    return 'modified_dump.bin'
}
