import React, { Component } from 'react'
import './App.css'
import { herokuWakeUp } from './service/heroku-wakeup-service'
import Routes from './components/Routes/Routes'

class App extends Component {
    componentDidMount(): void {
        herokuWakeUp()
    }

    render(): React.ReactElement {
        return (
            <div className="App">
                <Routes />
            </div>
        )
    }
}

export default App
