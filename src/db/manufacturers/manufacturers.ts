import opelVauxhallLogo from '../../assets/manufacturer/opelvauxhall.png'
import peugeotCitroenLogo from '../../assets/manufacturer/peugeotcitroen.png'
import {CarManufacturer, ManufacturerMenuItem} from '../../model/Manufacturer'

const carManufacturerMenu: ManufacturerMenuItem[] = [
  {
    imageUrl: opelVauxhallLogo,
    brand: 'Opel/Vauxhall',
    description: 'Decode radio or display',
    url: CarManufacturer.OPEL_VAUXHALL,
  },
  {
    imageUrl: peugeotCitroenLogo,
    brand: 'Peugeot/Citroen',
    description: 'Change radio vin...',
    url: CarManufacturer.PEUGEOT_CITROEN
  },
]

export default carManufacturerMenu
