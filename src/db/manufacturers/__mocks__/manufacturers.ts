import {CarManufacturer, Manufacturer, ManufacturerMenuItem} from '../../../model/Manufacturer'

export const CAR_MANUFACTURER: Manufacturer = {
  imageUrl: 'logo.url',
  brand: 'Opel/Vauxhall',
  description: 'Decode radio or display',
}

export const carManufacturerMenu: ManufacturerMenuItem[] = [
  {
    ...CAR_MANUFACTURER,
    url: CarManufacturer.OPEL_VAUXHALL,
  },
  {
    ...CAR_MANUFACTURER,
    url: CarManufacturer.OPEL_VAUXHALL,
  },
]

export default carManufacturerMenu
