import {DEVICE_INFO_MOCK} from '../../../mock/mock'
import {Device} from '../../../model/Device'

export const DEVICE_DETAILS_MOCK = {
  model: DEVICE_INFO_MOCK.deviceName,
  type: DEVICE_INFO_MOCK.deviceType,
  description: 'Extract security code and unregister eeprom dump',
  eepromType: '24C32 and 95640',
  image: 'image.url',
  procedureDescription: 'some procedure',
}

export const radioDevices: Device[] = [DEVICE_DETAILS_MOCK]
