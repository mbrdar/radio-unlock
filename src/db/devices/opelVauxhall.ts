import cd30Image from '../../assets/radio/cd30.png'
import cd70Image from '../../assets/radio/cd70.png'
import cdc40Image from '../../assets/radio/cdc40.jpg'
import cd400Image from '../../assets/radio/cd400.png'
import tidImage from '../../assets/display/tid.png'
import cidImage from '../../assets/display/cid.png'
import {DeviceType, Device} from '../../model/Device'

export enum OpelVauxhallRadioName {
  CD30 = 'CD30',
  CD70 = 'CD70',
  CDC40 = 'CDC40',
  CD400 = 'CD400'
}

export enum OpelVauxhallDisplayName {
  TID = 'TID',
  CID = 'CID'
}

export const opelRadios: Device[] = [
  {
    model: OpelVauxhallRadioName.CD30,
    type: DeviceType.RADIO,
    description: 'Extract security code and unregister eeprom dump',
    eepromType: '24C32 and 95640',
    image: cd30Image,
    procedureDescription: `There are two versions of the CD30 radio. <br>
    Both radios are supported:
    <ul>
        <li>Delphi Grunding CD30 eeprom type 24c32</li>
        <li>Blaupunkt CD30 eeprom type 95640</li>
    </ul>
    Read dump from eeprom chip. Select dump and click unregister.
    Modified dump will be created, write it to eeprom.
    Radio is now in delivery mode.
    Use opcom diagnostic tool to pair radio and display.
    Code for pairing is the one that you will get from display.
    `
  },
  {
    model: OpelVauxhallRadioName.CD70,
    type: DeviceType.RADIO,
    description: 'Extract security code and unregister eeprom dump',
    eepromType: '24C32',
    image: cd70Image,
    procedureDescription: `Delphi Grunding CD30 eeprom type 24c32
    <br><br>
    Read dump from eeprom chip. Select dump and click unregister.
    Modified dump will be created, write it to eeprom.
    Radio is now in delivery mode.
    Use opcom diagnostic tool to pair radio and display.
    Code for pairing is the one that you will get from display.
    `,
  },
  {
    model: OpelVauxhallRadioName.CDC40,
    type: DeviceType.RADIO,
    description: 'Extract security code and unregister eeprom dump',
    eepromType: '24c32',
    image: cdc40Image,
    procedureDescription: `There are two versions of the CD30 radio. <br>
    Both radios are supported:
    <ul>
        <li>Delphi Grunding CD30 eeprom type 24c32</li>
        <li>Blaupunkt CD30 eeprom type 95640</li>
    </ul>
    Read dump from eeprom chip. Select dump and click unregister.
    Modified dump will be created, write it to eeprom.
    Radio is now in delivery mode.
    Use opcom diagnostic tool to pair radio and display.
    Code for pairing is the one that you will get from display.
    `,
  },
  {
    model: OpelVauxhallRadioName.CD400,
    type: DeviceType.RADIO,
    description: 'Change vin and checksum correction',
    eepromType: ' 24c128',
    image: cd400Image,
    procedureDescription: `There are two versions of the CD400 radio. <br>
    Both radios are supported:
    <ul>
        <li>Daewoo Continental CD400 eeprom type 24c128</li>
        <li>Panasonic CD400 eeprom type 24c128</li>
    </ul>
    Read dump from eeprom chip. Select radio eeprom dump, enter correct car VIN number and click change vin.
    Modified dump will be created, write it to eeprom and thats all :)
    `,
  }
]

export const opelDisplays: Device[] = [
  {
    model: OpelVauxhallDisplayName.TID,
    type: DeviceType.DISPLAY,
    description: 'Extract security code, unregister and virgin eeprom dump',
    eepromType: ' 93c56 8bit',
    image: tidImage,
    procedureDescription: `
    There are several options available for this display.
    You can:
        <ul>
        <li>Extract security code</li>
        <li>Unregister eeprom dump</li>
        <li>Virginize eeprom dump</li>
    </ul>
    Read dump from 93c56 eeprom chip as 8 bit. Select dump and click on action that you want to perform.
    Modified dump will be created, write it to eeprom.
    Use opcom diagnostic tool to pair display with radio (Depending on the previous action).
    Code for pairing is the one that you will get from original car radio or car immo.
     `,
  },
  {
    model: OpelVauxhallDisplayName.CID,
    type: DeviceType.DISPLAY,
    description: 'CID/GID extract security code, unregister and virgin eeprom dump, reset counter',
    eepromType: '25080',
    image: cidImage,
    procedureDescription: `Both displays are supported:
    <ul>
        <li>GID eeprom type 25080</li>
        <li>CID eeprom type 25080</li>
    </ul>
    There are several options available for this display.
    You can:
        <ul>
        <li>Extract security code</li>
        <li>Unregister eeprom dump</li>
        <li>Virginize eeprom dump</li>
        <li>Reset counter</li>
    </ul>
    Read dump from eeprom chip. Select dump and click on action that you want to perform.
    Modified dump will be created, write it to eeprom.
    Use opcom diagnostic tool to pair display with radio (Depending on the previous action).
    Code for pairing is the one that you will get from original car radio or car immo.
    `,
  }
]
