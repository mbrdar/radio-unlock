import {Device} from '../../model/Device'
import {opelDisplays, opelRadios} from './opelVauxhall'
import {peugeotCitroenRadios} from './peugeotCitroen'

export const radioDevices: Device[] = [...opelRadios, ...peugeotCitroenRadios]

export const displayDeices: Device[] = [...opelDisplays]
