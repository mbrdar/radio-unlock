import {DeviceType, Device} from '../../model/Device'
import rd4m10eImage from '../../assets/radio/rd1n1m-03.png'

export enum PeugeotCitroenRadioName {
  RD4N1M_03 = 'RD4N1M-03',
}

export const peugeotCitroenRadios: Device[] = [
  {
    model: PeugeotCitroenRadioName.RD4N1M_03,
    type: DeviceType.RADIO,
    description: 'Change vin and checksum correction',
    eepromType: ' 24c64',
    image: rd4m10eImage,
    procedureDescription: `
    Read dump from 24c64 eeprom chip. Select radio eeprom dump, enter correct car VIN number and click change vin.
    Modified dump will be created, write it to eeprom and thats all :)
    `,
  }
]
