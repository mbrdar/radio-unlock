import {DecodingConfig} from '../../model/Decode'
import {opelVauxhallDisplayDecodingConfig, opelVauxhallRadioDecodingConfig} from './opelVauxhallDecondigConfig'
import {peugeotCitroenRadioDecodingConfig} from './peugeotCitroenDecondigConfig'

export const radioDecodingConfig: DecodingConfig[] = [
  ...opelVauxhallRadioDecodingConfig, ...peugeotCitroenRadioDecodingConfig
]

export const displayDecodingConfig: DecodingConfig[] = [
  ...opelVauxhallDisplayDecodingConfig
]
