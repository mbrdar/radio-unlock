import {DecodingAction, DecodingConfig} from '../../model/Decode'
import {PeugeotCitroenRadioName} from '../devices/peugeotCitroen'

export const peugeotCitroenRadioDecodingConfig: DecodingConfig[] = [
  {
    name: PeugeotCitroenRadioName.RD4N1M_03,
    actions: [DecodingAction.CHANGE_VIN]
  }
]
