import {DecodingAction, DecodingConfig} from '../../model/Decode'
import {OpelVauxhallDisplayName, OpelVauxhallRadioName} from '../devices/opelVauxhall'

const commonDecodingActions: DecodingAction[] = [
  DecodingAction.GET_INFO, DecodingAction.UNREGISTER
]

export const opelVauxhallRadioDecodingConfig: DecodingConfig[] = [
  {
    name: OpelVauxhallRadioName.CD30,
    actions: commonDecodingActions
  },
  {
    name: OpelVauxhallRadioName.CD70,
    actions: commonDecodingActions
  },
  {
    name: OpelVauxhallRadioName.CDC40,
    actions: commonDecodingActions
  },
  {
    name: OpelVauxhallRadioName.CD400,
    actions: [DecodingAction.CHANGE_VIN]
  }
]

export const opelVauxhallDisplayDecodingConfig: DecodingConfig[] = [
  {
    name: OpelVauxhallDisplayName.TID,
    actions: [...commonDecodingActions, DecodingAction.VIRGIN]
  },
  {
    name: OpelVauxhallDisplayName.CID,
    actions: [...commonDecodingActions, DecodingAction.VIRGIN, DecodingAction.RESET_COUNTER]
  },
]
