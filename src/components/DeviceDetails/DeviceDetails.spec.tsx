import * as api from './api'
import renderer from 'react-test-renderer'
import { DEVICE_INFO_MOCK } from '../../mock/mock'
import React from 'react'
import { DEVICE_DETAILS_MOCK } from '../../db/devices/__mocks__/devices'
import DeviceDetails from './DeviceDetails'

describe('Device details component:', () => {
    it('should render component', () => {
        jest.spyOn(api, 'default').mockReturnValue(DEVICE_DETAILS_MOCK)

        const component = renderer.create(<DeviceDetails {...DEVICE_INFO_MOCK} />)
        expect(component).toMatchSnapshot()
    })
})
