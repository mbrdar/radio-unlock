import {Device, DeviceName, DeviceType, InvalidRadioName} from '../../model/Device'
import {displayDeices, radioDevices} from '../../db/devices/devices'

const UNKNOWN_DEVICE: Device = {
  model: InvalidRadioName.INVALID,
  type: DeviceType.RADIO,
  description: 'Selected device is not supported',
  eepromType: 'Unknown',
  image: 'not existing',
  procedureDescription: 'Unknown',
}

const getDevice = (deviceType: DeviceType, deviceName: DeviceName): Device => {
  let device

  if (deviceType === DeviceType.RADIO) {
    device = radioDevices.find(device => device.model === deviceName)
  } else if (deviceType === DeviceType.DISPLAY) {
    device = displayDeices.find(device => device.model === deviceName)
  }
  return device || UNKNOWN_DEVICE
}

export default getDevice
