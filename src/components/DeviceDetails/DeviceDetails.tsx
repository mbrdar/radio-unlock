import React from 'react'
import { Box, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import parse from 'html-react-parser'
import getDevice from './api'
import { DeviceParams } from '../../model/Device'

const useStyles = makeStyles({
    devicePicture: {
        maxWidth: 300,
    },
})

const DeviceDetails: React.FC<DeviceParams> = ({ deviceType, deviceName }: DeviceParams) => {
    const { image, description, model, procedureDescription } = getDevice(deviceType, deviceName)
    const classes = useStyles()

    return (
        <div>
            <img src={image} alt="device" className={classes.devicePicture} />
            <Box mt={2}>
                <Typography variant="h5">{model}</Typography>
            </Box>
            <Box mb={2} mt={2}>
                <Typography variant={'overline'}>
                    <b>{description}</b>
                </Typography>
            </Box>
            <Box mb={2}>{parse(procedureDescription)}</Box>
        </div>
    )
}
export default DeviceDetails
