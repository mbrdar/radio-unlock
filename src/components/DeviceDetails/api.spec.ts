import getDevice from './api'
import {DEVICE_INFO_MOCK} from '../../mock/mock'
import {DEVICE_DETAILS_MOCK} from '../../db/devices/__mocks__/devices'
import {DeviceType, InvalidRadioName} from '../../model/Device'
import {OpelVauxhallRadioName} from '../../db/devices/opelVauxhall'

jest.mock('../../db/devices/devices')

describe('Device details api:', () => {
  it('should get device details', () => {
    const result = getDevice(DEVICE_INFO_MOCK.deviceType, DEVICE_INFO_MOCK.deviceName)
    expect(result).toEqual(DEVICE_DETAILS_MOCK)
  });

  it(`should get device when device doesn't exist`, () => {
    const result = getDevice(DeviceType.RADIO, OpelVauxhallRadioName.CD70)
    expect(result.model).toEqual(InvalidRadioName.INVALID)
  });
})
