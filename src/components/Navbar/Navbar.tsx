import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { Link } from 'react-router-dom'
import { Box } from '@material-ui/core'

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
    },
    title: {
        flexGrow: 1,
    },
}))

const Navbar: React.FC = () => {
    const classes = useStyles()

    return (
        <Box className={classes.root} mb={5}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        <Link to={'/'}>Radio unlock</Link>
                    </Typography>
                </Toolbar>
            </AppBar>
        </Box>
    )
}

export default Navbar
