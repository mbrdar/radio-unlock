import React from 'react'
import { Box } from '@material-ui/core'
import { DecodingAction } from '../../model/Decode'
import Info from '../Info/Info'
import Unregister from '../Unregister/Unregister'
import ChangeVin from '../ChangeVin/ChangeVin'
import Virgin from '../Virgin/Virgin'
import ResetCounter from '../ResetCounter/ResetCounter'
import getConfiguration from '../../containers/Decoder/api'
import { DeviceParams } from '../../model/Device'

type Props = DeviceParams & {
    file: File
}

const Action: React.FC<Props> = (props: Props) => {
    const config = getConfiguration(props.deviceType, props.deviceName)

    return (
        <Box>
            {config.actions.includes(DecodingAction.GET_INFO) ? (
                <Box mt={2}>
                    <Info {...props} />
                </Box>
            ) : (
                ''
            )}
            {config.actions.includes(DecodingAction.UNREGISTER) ? (
                <Box mt={2}>
                    <Unregister {...props} />
                </Box>
            ) : (
                ''
            )}
            {config.actions.includes(DecodingAction.VIRGIN) ? (
                <Box mt={2}>
                    <Virgin {...props} />
                </Box>
            ) : (
                ''
            )}
            {config.actions.includes(DecodingAction.CHANGE_VIN) ? (
                <Box mt={2}>
                    <ChangeVin {...props} />
                </Box>
            ) : (
                ''
            )}
            {config.actions.includes(DecodingAction.RESET_COUNTER) ? (
                <Box mt={2}>
                    <ResetCounter {...props} />
                </Box>
            ) : (
                ''
            )}
        </Box>
    )
}
export default Action
