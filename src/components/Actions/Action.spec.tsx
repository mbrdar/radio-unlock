import React from 'react'
import { DEVICE_INFO_MOCK } from '../../mock/mock'
import * as configProvider from '../../containers/Decoder/api'
import { DecodingAction, DecodingConfig } from '../../model/Decode'
import { OpelVauxhallRadioName } from '../../db/devices/opelVauxhall'
import Action from './Action'
import { createRenderer, ShallowRenderer } from 'react-test-renderer/shallow'

describe('Action component: ', () => {
    let shallowRenderer: ShallowRenderer

    beforeEach(() => {
        shallowRenderer = createRenderer()
    })

    it('should renders all components', () => {
        const decodingConfig: DecodingConfig = {
            name: OpelVauxhallRadioName.CD30,
            actions: [
                DecodingAction.CHANGE_VIN,
                DecodingAction.GET_INFO,
                DecodingAction.RESET_COUNTER,
                DecodingAction.UNREGISTER,
                DecodingAction.VIRGIN,
            ],
        }
        jest.spyOn(configProvider, 'default').mockReturnValue(decodingConfig)

        shallowRenderer.render(<Action {...DEVICE_INFO_MOCK} />)
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
    })

    it('should renders only one components', () => {
        const decodingConfig: DecodingConfig = {
            name: OpelVauxhallRadioName.CD30,
            actions: [DecodingAction.GET_INFO],
        }

        jest.spyOn(configProvider, 'default').mockReturnValue(decodingConfig)

        shallowRenderer.render(<Action {...DEVICE_INFO_MOCK} />)
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
    })
})
