import React, { useEffect, useState } from 'react'
import ErrorDialog from '../ErrorDialog/ErrorDialog'
import getDumpInfo from './api'
import { DumpInfo } from '../../model/DumpInfo'
import { DeviceParams } from '../../model/Device'
import getErrorMessage from '../../service/error-handler'

type Props = DeviceParams & {
    file: File | undefined
}

const Info: React.FC<Props> = ({ deviceType, deviceName, file }: Props) => {
    const [info, setInfo] = useState<DumpInfo>({ code: '' })
    const [errorMessage, setErrorMessage] = React.useState('')

    useEffect(() => {
        if (file) {
            getDumpInfo(deviceType, deviceName, file).then(
                (dumpInfo) => {
                    setInfo(dumpInfo)
                },
                (error) => {
                    setErrorMessage(
                        getErrorMessage(error, 'Unable to get info from eeprom dump! Check is dump correct.'),
                    )
                },
            )
        }
    }, [deviceType, deviceName, file])

    return (
        <div>
            <p>{info.code ? 'Code: ' + info.code : ''}</p>
            <p>{info.counter ? 'Counter: ' + info.counter : ''}</p>
            <p>{info.serialNumber ? 'Serial number: ' + info.serialNumber : ''}</p>
            <ErrorDialog message={errorMessage} />
        </div>
    )
}
export default Info
