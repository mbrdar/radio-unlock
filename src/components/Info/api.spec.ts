import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {DumpInfo} from '../../model/DumpInfo'
import getDumpInfo from './api'
import {TEST_API_URL} from '../../service/__mocks__/environment'
import {DEVICE_INFO_MOCK} from '../../mock/mock'

jest.mock('../../service/environment');

describe('Info api:', () => {
  it('returns dump info', done => {
    const mock = new MockAdapter(axios);

    const expectedResponse: DumpInfo = {code: '0984'}

    const formData = new FormData();
    formData.append('file', DEVICE_INFO_MOCK.file)

    mock.onPost(`${TEST_API_URL}/${DEVICE_INFO_MOCK.deviceType}/${DEVICE_INFO_MOCK.deviceName.toLocaleLowerCase()}/info`)
      .reply((request) => {
        expect(request.data).toEqual(formData);
        return [200, expectedResponse]
      })

    getDumpInfo(DEVICE_INFO_MOCK.deviceType, DEVICE_INFO_MOCK.deviceName, DEVICE_INFO_MOCK.file).then(response => {
      expect(response).toEqual(expectedResponse)
      done()
    })
  })
})
