import React from 'react'
import * as api from './api'
import { DumpInfo } from '../../model/DumpInfo'
import renderer, { act, ReactTestRenderer } from 'react-test-renderer'
import Info from './Info'
import { DEVICE_INFO_MOCK } from '../../mock/mock'
import { createRenderer } from 'react-test-renderer/shallow'

describe('Info component: ', () => {
    it('should render correctly', async (done) => {
        const dumpInfo: DumpInfo = {
            code: '0956',
            counter: 10,
            serialNumber: 'WER12345678998765',
        }

        const apiCallSpy = jest.spyOn(api, 'default').mockResolvedValueOnce(dumpInfo)

        let component: ReactTestRenderer

        await act(async () => {
            component = renderer.create(
                <Info
                    deviceName={DEVICE_INFO_MOCK.deviceName}
                    deviceType={DEVICE_INFO_MOCK.deviceType}
                    file={DEVICE_INFO_MOCK.file}
                />,
            )
        }).then(() => {
            expect(apiCallSpy).toBeCalledWith(
                DEVICE_INFO_MOCK.deviceType,
                DEVICE_INFO_MOCK.deviceName,
                DEVICE_INFO_MOCK.file,
            )
            expect(component.toJSON()).toMatchSnapshot()
            apiCallSpy.mockRestore()
            done()
        })
    })

    // TODO at the moment triggering hooks is not supported during shallow rendering
    xit('should show error', async () => {
        const shallowRenderer = createRenderer()
        const errorResponse = {
            response: {
                data: {
                    message: 'Error message',
                },
            },
        }

        const apiCallSpy = jest.spyOn(api, 'default').mockRejectedValueOnce(errorResponse)

        await act(async () => {
            shallowRenderer.render(
                <Info
                    deviceName={DEVICE_INFO_MOCK.deviceName}
                    deviceType={DEVICE_INFO_MOCK.deviceType}
                    file={DEVICE_INFO_MOCK.file}
                />,
            )
        })

        expect(apiCallSpy).toBeCalledWith(
            DEVICE_INFO_MOCK.deviceType,
            DEVICE_INFO_MOCK.deviceName,
            DEVICE_INFO_MOCK.file,
        )
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
        apiCallSpy.mockRestore()
    })
})
