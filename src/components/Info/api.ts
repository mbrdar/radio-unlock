import {DeviceName, DeviceType} from '../../model/Device'
import axios from 'axios'
import {apiUrl} from '../../service/environment'
import {DumpInfo} from '../../model/DumpInfo'

const getDumpInfo = async (deviceType: DeviceType, deviceName: DeviceName, file: File): Promise<DumpInfo> => {
  const formData = new FormData();
  formData.append('file', file)
  const response = await axios.post(`${apiUrl()}/${deviceType}/${deviceName.toLocaleLowerCase()}/info`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
  return response.data as DumpInfo
}

export default getDumpInfo
