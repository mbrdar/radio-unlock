import React from 'react'
import renderer from 'react-test-renderer'
import { CarManufacturer, Manufacturer } from '../../model/Manufacturer'
import Item from './Item'

it('renders correctly', () => {
    const item = {
        imageUrl: 'test.png',
        brand: CarManufacturer.OPEL_VAUXHALL,
        description: 'some description',
    } as Manufacturer

    const tree = renderer
        .create(<Item image={item.imageUrl} description={item.description} name={item.brand} />)
        .toJSON()
    expect(tree).toMatchSnapshot()
})
