import renderer from 'react-test-renderer'
import React from 'react'
import Routes from './Routes'

describe('Routes component:', () => {
    it('should render', () => {
        expect(renderer.create(<Routes />).toJSON()).toMatchSnapshot()
    })
})
