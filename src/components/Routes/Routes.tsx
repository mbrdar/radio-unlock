import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import CarManufacturerList from '../../containers/CarManufacturerList/CarManufacturerList'
import DeviceList from '../../containers/DeviceList/DeviceList'
import Decoder from '../../containers/Decoder/Decoder'
import { Container } from '@material-ui/core'
import Navbar from '../Navbar/Navbar'

const Routes: React.FC = () => {
    return (
        <BrowserRouter basename={process.env.PUBLIC_URL}>
            <Navbar />
            <Container>
                <Switch>
                    <Route exact path={'/'} render={() => <CarManufacturerList />} />
                    <Route exact path={'/:manufacturerType'} render={(props) => <DeviceList {...props} />} />
                    <Route exact path={'/:deviceType/:deviceName'} render={(props) => <Decoder {...props} />} />
                </Switch>
            </Container>
        </BrowserRouter>
    )
}
export default Routes
