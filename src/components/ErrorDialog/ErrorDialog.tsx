import React, { useEffect } from 'react'
import { Snackbar } from '@material-ui/core'
import MuiAlert from '@material-ui/lab/Alert'

type Props = {
    message: string
}

const ErrorDialog: React.FC<Props> = ({ message }: Props) => {
    const [open, setOpen] = React.useState(false)

    useEffect(() => {
        if (message) {
            setOpen(true)
        }
    }, [message])

    const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return
        }

        setOpen(false)
    }

    return (
        <Snackbar
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
        >
            <MuiAlert onClose={handleClose} severity={'error'}>
                {message}
            </MuiAlert>
        </Snackbar>
    )
}
export default ErrorDialog
