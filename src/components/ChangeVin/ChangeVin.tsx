import React, { ChangeEvent } from 'react'
import { Box, Button, createStyles, TextField } from '@material-ui/core'
import { ErrorResponse } from '../../model/ErrorResponse'
import ErrorDialog from '../ErrorDialog/ErrorDialog'
import { makeStyles } from '@material-ui/core/styles'
import { changeVin } from './api'
import { DeviceParams } from '../../model/Device'

type Props = DeviceParams & {
    file: File
}

const useStyles = makeStyles(() =>
    createStyles({
        input: {
            width: 300,
        },
    }),
)

const ChangeVin: React.FC<Props> = ({ deviceType, deviceName, file }: Props) => {
    const [errorMessage, setErrorMessage] = React.useState('')
    const [vin, setVin] = React.useState('')
    const classes = useStyles()

    const handleClick = () => {
        if (vin.length === 17) {
            changeVin(deviceType, deviceName, file, vin).catch((error) => {
                const serverError = error.response.data as ErrorResponse
                setErrorMessage(serverError.message || 'Unable to unregister eeprom dump!')
            })
        } else {
            setErrorMessage('VIN should be 17 characters long')
        }
    }

    const handleVinChange = (event: ChangeEvent<HTMLInputElement>) => {
        setVin(event.target.value)
    }

    return (
        <Box mt={4}>
            <Box>
                <TextField
                    label="VIN"
                    variant="outlined"
                    className={classes.input}
                    inputProps={{ maxLength: 17 }}
                    onChange={handleVinChange}
                />
            </Box>
            <Box mt={2}>
                <Button variant="contained" color="primary" size={'large'} onClick={handleClick}>
                    Change vin
                </Button>
            </Box>
            <ErrorDialog message={errorMessage} />
        </Box>
    )
}
export default ChangeVin
