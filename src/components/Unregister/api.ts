import {DeviceName, DeviceType} from '../../model/Device'
import axios from 'axios'
import {downloadFile, getFileNameFromContentDispositionHeader} from '../../service/download-service'
import {apiUrl} from '../../service/environment'

export const unregister = async (deviceType: DeviceType, deviceName: DeviceName, file: File): Promise<void> => {
  const formData = new FormData();
  formData.append('file', file)
  const response = await axios.post(`${apiUrl()}/${deviceType}/${deviceName.toLocaleLowerCase()}/unregister`, formData, {
    responseType: 'arraybuffer',
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
  downloadFile([response.data], getFileNameFromContentDispositionHeader(response.headers['content-disposition']))
}
