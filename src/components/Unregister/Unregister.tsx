import React from 'react'
import { Box, Button } from '@material-ui/core'
import ErrorDialog from '../ErrorDialog/ErrorDialog'
import { unregister } from './api'
import { DeviceParams } from '../../model/Device'
import getErrorMessage from '../../service/error-handler'

type Props = DeviceParams & {
    file: File
}

const Unregister: React.FC<Props> = ({ deviceType, deviceName, file }: Props) => {
    const [errorMessage, setErrorMessage] = React.useState('')
    const handleClick = () => {
        unregister(deviceType, deviceName, file).catch((error) => {
            setErrorMessage(getErrorMessage(error, 'Unable to unregister eeprom dump!'))
        })
    }

    return (
        <Box>
            <Button variant="contained" color="primary" onClick={handleClick}>
                Unregister
            </Button>
            <ErrorDialog message={errorMessage} />
        </Box>
    )
}
export default Unregister
