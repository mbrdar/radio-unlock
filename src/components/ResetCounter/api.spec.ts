import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {TEST_API_URL} from '../../service/__mocks__/environment'
import {DEVICE_INFO_MOCK} from '../../mock/mock'
import {resetCounter} from './api'
import * as downloadService from '../../service/download-service'

jest.mock('../../service/environment');

describe('Reset counter api:', () => {
  it('should reset dump counter', done => {
    const mock = new MockAdapter(axios);
    const expectedFileName = 'filename.bin'
    const contentDispositionHeaderValue = `Content-Disposition: attachment; filename="${expectedFileName}"`

    const byteArray = new ArrayBuffer(16)

    const formData = new FormData();
    formData.append('file', DEVICE_INFO_MOCK.file)

    const fileNameSpy = jest.spyOn(downloadService, 'getFileNameFromContentDispositionHeader').mockReturnValue(expectedFileName)
    const downloadFileSpy = jest.spyOn(downloadService, 'downloadFile').mockImplementation(() => {})

    mock.onPost(`${TEST_API_URL}/${DEVICE_INFO_MOCK.deviceType}/${DEVICE_INFO_MOCK.deviceName.toLocaleLowerCase()}/reset-counter`)
      .reply((request) => {
        expect(request.data).toEqual(formData);
        return [200, byteArray, {'content-disposition': contentDispositionHeaderValue}]
      })

    resetCounter(DEVICE_INFO_MOCK.deviceType, DEVICE_INFO_MOCK.deviceName, DEVICE_INFO_MOCK.file).then(_ => {
      expect(fileNameSpy).toHaveBeenCalledWith(contentDispositionHeaderValue)
      expect(downloadFileSpy).toHaveBeenCalledWith([byteArray], expectedFileName)
      done()
    })
  })
})
