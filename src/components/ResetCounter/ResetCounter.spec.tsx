import renderer, { act } from 'react-test-renderer'
import { DEVICE_INFO_MOCK } from '../../mock/mock'
import React from 'react'
import ResetCounter from './ResetCounter'
import * as api from './api'
import { createRenderer } from 'react-test-renderer/shallow'

describe('ResetCounter component: ', () => {
    it('should render component', () => {
        expect(renderer.create(<ResetCounter {...DEVICE_INFO_MOCK} />).toJSON()).toMatchSnapshot()
    })

    it('should reset counter on button click', async () => {
        const shallowRenderer = createRenderer()

        const apiCallSpy = jest.spyOn(api, 'resetCounter').mockResolvedValueOnce()

        shallowRenderer.render(<ResetCounter {...DEVICE_INFO_MOCK} />)
        const component = shallowRenderer.getRenderOutput()

        await act(async () => {
            component.props.children[0].props.onClick()
        })

        expect(apiCallSpy).toBeCalledWith(
            DEVICE_INFO_MOCK.deviceType,
            DEVICE_INFO_MOCK.deviceName,
            DEVICE_INFO_MOCK.file,
        )
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
    })

    it('should reset counter on error', async () => {
        const shallowRenderer = createRenderer()
        const apiCallSpy = jest.spyOn(api, 'resetCounter').mockRejectedValueOnce({})

        shallowRenderer.render(<ResetCounter {...DEVICE_INFO_MOCK} />)
        const component = shallowRenderer.getRenderOutput()

        await act(async () => {
            component.props.children[0].props.onClick()
        })

        expect(apiCallSpy).toBeCalledWith(
            DEVICE_INFO_MOCK.deviceType,
            DEVICE_INFO_MOCK.deviceName,
            DEVICE_INFO_MOCK.file,
        )
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
    })
})
