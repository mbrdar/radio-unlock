import React from 'react'
import { Box, Button } from '@material-ui/core'
import ErrorDialog from '../ErrorDialog/ErrorDialog'
import { resetCounter } from './api'
import { DeviceParams } from '../../model/Device'
import getErrorMessage from '../../service/error-handler'

type Props = DeviceParams & {
    file: File
}

const ResetCounter: React.FC<Props> = ({ deviceType, deviceName, file }: Props) => {
    const [errorMessage, setErrorMessage] = React.useState('')
    const handleClick = () => {
        resetCounter(deviceType, deviceName, file).catch((error) => {
            setErrorMessage(getErrorMessage(error, 'Unable to reset counter on eeprom dump!'))
        })
    }

    return (
        <Box>
            <Button variant="contained" color="primary" onClick={handleClick}>
                Reset counter
            </Button>
            <ErrorDialog message={errorMessage} />
        </Box>
    )
}
export default ResetCounter
