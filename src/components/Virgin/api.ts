import axios from 'axios'
import {DeviceName, DeviceType} from '../../model/Device'
import {apiUrl} from '../../service/environment'
import {downloadFile, getFileNameFromContentDispositionHeader} from '../../service/download-service'

export const virgin = async (deviceType: DeviceType, deviceName: DeviceName, file: File): Promise<void> => {
  const formData = new FormData();
  formData.append('file', file)
  const response = await axios.post(`${apiUrl()}/${deviceType}/${deviceName.toLocaleLowerCase()}/virgin`, formData, {
    responseType: 'arraybuffer',
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
  downloadFile([response.data], getFileNameFromContentDispositionHeader(response.headers['content-disposition']))
}
