import renderer, { act } from 'react-test-renderer'
import { DEVICE_INFO_MOCK } from '../../mock/mock'
import React from 'react'
import * as api from './api'
import { createRenderer } from 'react-test-renderer/shallow'
import Virgin from './Virgin'

describe('Virgin component: ', () => {
    it('should render component', () => {
        expect(renderer.create(<Virgin {...DEVICE_INFO_MOCK} />).toJSON()).toMatchSnapshot()
    })

    it('should virgin dump on button click', async () => {
        const shallowRenderer = createRenderer()

        const apiCallSpy = jest.spyOn(api, 'virgin').mockResolvedValueOnce()

        shallowRenderer.render(<Virgin {...DEVICE_INFO_MOCK} />)
        const component = shallowRenderer.getRenderOutput()

        await act(async () => {
            component.props.children[0].props.onClick()
        })

        expect(apiCallSpy).toBeCalledWith(
            DEVICE_INFO_MOCK.deviceType,
            DEVICE_INFO_MOCK.deviceName,
            DEVICE_INFO_MOCK.file,
        )
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
    })

    it('should virgin dump on error', async () => {
        const shallowRenderer = createRenderer()
        const apiCallSpy = jest.spyOn(api, 'virgin').mockRejectedValueOnce({})

        shallowRenderer.render(<Virgin {...DEVICE_INFO_MOCK} />)
        const component = shallowRenderer.getRenderOutput()

        await act(async () => {
            component.props.children[0].props.onClick()
        })

        expect(apiCallSpy).toBeCalledWith(
            DEVICE_INFO_MOCK.deviceType,
            DEVICE_INFO_MOCK.deviceName,
            DEVICE_INFO_MOCK.file,
        )
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
    })
})
