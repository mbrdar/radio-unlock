import React, { useState } from 'react'
import { Box, Divider, Grid } from '@material-ui/core'
import { RouteComponentProps } from 'react-router'
import { DeviceParams, InvalidRadioName } from '../../model/Device'
import DeviceDetails from '../../components/DeviceDetails/DeviceDetails'
import { DropzoneArea } from 'material-ui-dropzone'
import Action from '../../components/Actions/Action'
import getConfiguration from './api'

type Props = RouteComponentProps<DeviceParams>

const Decoder: React.FC<Props> = (props: Props) => {
    const [file, setFile] = useState<File>()
    const deviceType = props.match.params.deviceType
    const deviceName = props.match.params.deviceName
    const configuration = getConfiguration(props.match.params.deviceType, props.match.params.deviceName)

    const handleChange = (files: File[]): void => {
        setFile(files[0])
    }

    return (
        <Box>
            {configuration.name === InvalidRadioName.INVALID ? (
                <p>Invalid device</p>
            ) : (
                <div>
                    <Grid container spacing={6}>
                        <Grid item xs={12} sm={6}>
                            <Box>
                                <DropzoneArea
                                    onChange={handleChange}
                                    acceptedFiles={['.bin']}
                                    showPreviews={true}
                                    showPreviewsInDropzone={false}
                                    maxFileSize={160000}
                                    filesLimit={1}
                                    useChipsForPreview={true}
                                    dropzoneText={'Drop eeprom dump or click to add it'}
                                    showFileNames={true}
                                    showAlerts={false}
                                />
                            </Box>
                            <Box>
                                <br />
                                <Divider orientation={'horizontal'} />
                                {file ? <Action deviceType={deviceType} deviceName={deviceName} file={file} /> : ''}
                            </Box>
                        </Grid>
                        <Divider orientation={'vertical'} flexItem />
                        <Grid item xs={12} sm={5}>
                            <Box flexDirection={'row'}>
                                <DeviceDetails deviceType={deviceType} deviceName={deviceName} />
                            </Box>
                        </Grid>
                    </Grid>
                </div>
            )}
        </Box>
    )
}
export default Decoder
