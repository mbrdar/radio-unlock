import {DeviceName, DeviceType, InvalidRadioName} from '../../model/Device'
import {DecodingConfig} from '../../model/Decode'
import {displayDecodingConfig, radioDecodingConfig} from '../../db/config/radioDecodingConfig'

const getConfiguration = (deviceType: DeviceType, deviceName: DeviceName): DecodingConfig => {
  let device

  if (deviceType === DeviceType.RADIO) {
    device = radioDecodingConfig.find(device => device.name === deviceName)
  } else if (deviceType === DeviceType.DISPLAY) {
    device = displayDecodingConfig.find(device => device.name === deviceName)
  }
  return device || {name: InvalidRadioName.INVALID, actions: []}
}

export default getConfiguration
