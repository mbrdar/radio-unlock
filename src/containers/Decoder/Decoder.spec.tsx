/* eslint-disable  @typescript-eslint/no-explicit-any */
import { createRenderer, ShallowRenderer } from 'react-test-renderer/shallow'
import React from 'react'
import * as api from './api'
import { DEVICE_INFO_MOCK } from '../../mock/mock'
import { DecodingAction, DecodingConfig } from '../../model/Decode'
import { OpelVauxhallRadioName } from '../../db/devices/opelVauxhall'
import Decoder from './Decoder'
import { InvalidRadioName } from '../../model/Device'
import { act } from 'react-test-renderer'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json/index'

describe('Decoder component:', () => {
    let shallowRenderer: ShallowRenderer
    let configuration: DecodingConfig

    beforeEach(() => {
        shallowRenderer = createRenderer()
        configuration = {
            name: OpelVauxhallRadioName.CD30,
            actions: [DecodingAction.GET_INFO],
        }
    })

    it('should render', () => {
        jest.spyOn(api, 'default').mockReturnValue(configuration)

        const routeComponentProps = {
            history: {} as any,
            location: {} as any,
            match: {
                params: {
                    deviceType: DEVICE_INFO_MOCK.deviceType,
                    deviceName: DEVICE_INFO_MOCK.deviceName,
                },
            } as any,
        }

        shallowRenderer.render(<Decoder {...routeComponentProps} />)
        expect(api.default).toHaveBeenCalledWith(DEVICE_INFO_MOCK.deviceType, DEVICE_INFO_MOCK.deviceName)
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
    })

    it('should render when device is unknown', () => {
        configuration = { name: InvalidRadioName.INVALID, actions: [] }

        jest.spyOn(api, 'default').mockReturnValue(configuration)

        const routeComponentProps = {
            history: {} as any,
            location: {} as any,
            match: {
                params: {
                    deviceType: DEVICE_INFO_MOCK.deviceType,
                    deviceName: DEVICE_INFO_MOCK.deviceName,
                },
            } as any,
        }

        shallowRenderer.render(<Decoder {...routeComponentProps} />)
        expect(api.default).toHaveBeenCalledWith(DEVICE_INFO_MOCK.deviceType, DEVICE_INFO_MOCK.deviceName)
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
    })

    it('should select file', async () => {
        jest.spyOn(api, 'default').mockReturnValue(configuration)

        const routeComponentProps = {
            history: {} as any,
            location: {} as any,
            match: {
                params: {
                    deviceType: DEVICE_INFO_MOCK.deviceType,
                    deviceName: DEVICE_INFO_MOCK.deviceName,
                },
            } as any,
        }

        const component = shallow(<Decoder {...routeComponentProps} />)

        await act(async () => {
            component.find('DropzoneArea').simulate('change', [DEVICE_INFO_MOCK.file])
        })

        expect(api.default).toHaveBeenCalledWith(DEVICE_INFO_MOCK.deviceType, DEVICE_INFO_MOCK.deviceName)
        expect(shallowToJson(component)).toMatchSnapshot()
    })
})
