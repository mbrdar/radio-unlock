import React from 'react'
import { Link } from 'react-router-dom'
import { createStyles, Grid, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Item from '../../components/Item/Item'
import carManufacturerMenu from '../../db/manufacturers/manufacturers'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        control: {
            padding: theme.spacing(2),
        },
    }),
)

const CarManufacturerList: React.FC = () => {
    const classes = useStyles()

    return (
        <Grid container className={classes.root} spacing={4} justify={'center'}>
            {carManufacturerMenu.map((menuItem, index) => (
                <Grid key={index} item xs={12} sm={6} md={4}>
                    <Link to={menuItem.url}>
                        <Item
                            key={index}
                            image={menuItem.imageUrl}
                            name={menuItem.brand}
                            description={menuItem.description}
                        />
                    </Link>
                </Grid>
            ))}
        </Grid>
    )
}
export default CarManufacturerList
