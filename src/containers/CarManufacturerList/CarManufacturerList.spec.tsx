import { createRenderer, ShallowRenderer } from 'react-test-renderer/shallow'
import CarManufacturerList from './CarManufacturerList'
import React from 'react'
jest.mock('../../db/manufacturers/manufacturers')

describe('Car manufacturer list component:', () => {
    let shallowRenderer: ShallowRenderer

    beforeEach(() => {
        shallowRenderer = createRenderer()
    })

    it('should render', () => {
        shallowRenderer.render(<CarManufacturerList />)
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
    })
})
