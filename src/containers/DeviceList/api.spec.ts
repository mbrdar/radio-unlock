import getRadios from './api'
import {CarManufacturer} from '../../model/Manufacturer'

jest.mock('../../db/devices/opelVauxhall', () => ({
  opelRadios: [{}],
  opelDisplays: [{}],
}))

describe('Device list api:', () => {
  it('should get devices', () => {
    const result = getRadios(CarManufacturer.OPEL_VAUXHALL)
    expect(result.length).toEqual(2)
  });
})
