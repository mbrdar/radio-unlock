import React from 'react'
import { createStyles, Grid, Theme } from '@material-ui/core'
import { RouteComponentProps } from 'react-router'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import getRadios from './api'
import { CarManufacturer } from '../../model/Manufacturer'
import Item from '../../components/Item/Item'

interface MatchParams {
    manufacturerType: CarManufacturer
}

type Props = RouteComponentProps<MatchParams>

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        control: {
            padding: theme.spacing(2),
        },
    }),
)

const DeviceList: React.FC<Props> = (props: Props) => {
    const radios = getRadios(props.match.params.manufacturerType)
    const classes = useStyles()
    return (
        <Grid container className={classes.root} spacing={5} justify={'center'}>
            {radios.map((radioItem, index) => (
                <Grid key={index} item xs={12} sm={6} md={4}>
                    <Link to={radioItem.type + '/' + radioItem.model}>
                        <Item
                            key={index}
                            name={radioItem.model}
                            description={radioItem.description}
                            image={radioItem.image}
                        />
                    </Link>
                </Grid>
            ))}
        </Grid>
    )
}
export default DeviceList
