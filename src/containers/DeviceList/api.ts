import {Device} from '../../model/Device'
import {opelDisplays, opelRadios} from '../../db/devices/opelVauxhall'
import {peugeotCitroenRadios} from '../../db/devices/peugeotCitroen'
import {CarManufacturer} from '../../model/Manufacturer'

const getRadios = (carManufacturer: CarManufacturer): Device[] => {
  switch (carManufacturer) {
    case CarManufacturer.OPEL_VAUXHALL:
      return [...opelRadios, ...opelDisplays]
    case CarManufacturer.PEUGEOT_CITROEN:
      return peugeotCitroenRadios
  }
}

export default getRadios
