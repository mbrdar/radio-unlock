/* eslint-disable  @typescript-eslint/no-explicit-any */
import { createRenderer, ShallowRenderer } from 'react-test-renderer/shallow'
import React from 'react'
import DeviceList from './DeviceList'
import { CAR_MANUFACTURER } from '../../db/manufacturers/__mocks__/manufacturers'
import * as api from './api'
import { radioDevices } from '../../db/devices/__mocks__/devices'

describe('Car manufacturer list component:', () => {
    let shallowRenderer: ShallowRenderer

    beforeEach(() => {
        shallowRenderer = createRenderer()
    })

    it('should render', () => {
        const routeComponentProps = {
            history: {} as any,
            location: {} as any,
            match: {
                params: {
                    manufacturerType: CAR_MANUFACTURER.brand,
                },
            } as any,
        }

        jest.spyOn(api, 'default').mockReturnValue(radioDevices)

        shallowRenderer.render(<DeviceList {...routeComponentProps} />)
        expect(api.default).toHaveBeenCalledWith(routeComponentProps.match.params.manufacturerType)
        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
    })
})
