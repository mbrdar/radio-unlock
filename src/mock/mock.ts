import { DeviceType } from '../model/Device'
import { OpelVauxhallRadioName } from '../db/devices/opelVauxhall'

export const DEVICE_INFO_MOCK = {
    deviceType: DeviceType.RADIO,
    deviceName: OpelVauxhallRadioName.CD30,
    file: new File([''], 'dump.bin'),
}
